/*
  BlackBox MQTT testing firmware

  Creates connection and publish packets and publishes to the server through MQTT

  Amjed Nizam
  Utech
*/
//Git test
//Server details
char server[] = "utechconnect.tk";
char port[] = "8883";

/////// CONN packet variables
char client_ID[] = "cZsOv1YWRpZMKm3U2Z6j";          //!!cannot be 26 letters!!
char MQTT_username[] = "utechiot";
char MQTT_password[] = "Utech@1";
int keep_alive_time = 120;            //session keep alive time in seconds
int conn_packet_size = 0;             //global variable. updated in make_conn_packet()
int sub_packet_size = 0;
/////// PUBLISH packet variables
byte QoS = 1;
char topic[]= "messages/cZsOv1YWRpZMKm3U2Z6j";
char apiToken[] = "cZsOv1YWRpZMKm3U2Z6j";
char message[] = "{\"data_set\":{\"temperature\":31,\"humidity\":50}}";

//char message[] = "{\"api_token\":\"cZsOv1YWRpZMKm3U2Z6j\",\"data_set\":{\"AN01\":0,\"AN02\":0,\"AN03\":0,\"AN04\":15,\"AN05\":0,\"AN06\":0,\"AN07\":0,\"AN08\":0,\"AN09\":1,\"AN10\":6562545,\"AN11\":0,\"AN12\":0,\"AN13\":0,\"DI01\":0,\"DI02\":0,\"DI03\":0,\"DI04\":0,\"DI05\":0,\"DI06\":0,\"LOGIC01\":0,\"LOGIC02\":0,\"LOGIC03\":0,\"LOGIC04\":0,\"LOGIC05\":0,\"EQUIP_CONDITION\":12575,\"DEVICE_STATUS\":4066,\"HOUR_METER\":651.57,\"GPS_LOCATION\":\"18.453445,-66.303085\",\"GPS_SPEED\":28.71,\"SUPPLY_VOLTAGE\":1606.39,\"SIGNAL_STRENGTH\":39},\"const\":{\"SUPV\":1606.39,\"BATV\":1.17,\"UTIM\":0,\"DSTA\":0,\"CFLG\":12575,\"SFLG\":4066,\"ANLP\":0,\"DGIP\":0,\"MBAC\":0,\"RSSI\":39,\"NMCC\":0,\"NMNC\":0,\"NLAC\":0,\"NCID\":0,\"OTAS\":0}}";

int pub_packet_size;

/////// Parameter variables
int32_t paraValues[64]= {0};
int modCan = 0;
int mcc=0;
int mnc=0;
int lac=0;
int cellid=0;



//byte mqttconnect[] = {0x10,0x25,0x00,0x04,0x4D,0x51,0x54,0x54,0x04,0xC2,0x00,0x78,0x00,0x06,0x41,0x42,0x43,0x44,0x45,0x46,0x00,0x08,0x75,0x74,0x65,0x63,0x68,0x69,0x6F,0x74,0x00,0x07,0x55,0x74,0x65,0x63,0x68,0x40,0x31};
//byte publish_qos1[] = {0x32,0x47,0x00,0x16,0x6D,0x65,0x73,0x73,0x61,0x67,0x65,0x73,0x2F,0x54,0x31,0x5F,0x54,0x45,0x53,0x54,0x5F,0x54,0x4F,0x4B,0x45,0x4E,0x00,0x04,0x7B,0x22,0x64,0x61,0x74,0x61,0x5F,0x73,0x65,0x74,0x22,0x3A,0x7B,0x22,0x74,0x65,0x6D,0x70,0x65,0x72,0x61,0x74,0x75,0x72,0x65,0x22,0x3A,0x31,0x36,0x2C,0x22,0x68,0x75,0x6D,0x69,0x64,0x69,0x74,0x79,0x22,0x3A,0x32,0x37,0x7D,0x7D};

byte* make_conn_packet(){
  
  static byte temp_packet[100];
  int count=0;
  int i =0;
  int j =0;
  temp_packet[0] = 0x10;                                      //Packet identifier 10 (CONN)

  temp_packet[2] = 0x00;                                      //Protocol length
  temp_packet[3] = 0x04;

  temp_packet[4] = 0x4D;                                      //M
  temp_packet[5] = 0x51;                                      //Q
  temp_packet[6] = 0x54;                                      //T
  temp_packet[7] = 0x54;                                      //T

  temp_packet[8] = 0x04;                                      //Protocol level

  temp_packet[9] = 0xC2;                                      //Flag bit

  temp_packet[10] = 0x00;                                     //Keep alive
  temp_packet[11] = keep_alive_time;

  temp_packet[12] = 0x00;                                     // Client ID length
  temp_packet[13] = (sizeof(client_ID)-1);
  
  for(count=14; count<(sizeof(client_ID)-1+14); count++){        // Client ID
    temp_packet[count] = client_ID[count-14];
  }
  Serial.print("count - ");
  Serial.println(count);
  
  temp_packet[count] = 0x00;                                  //username length
  count++;
  temp_packet[count] = (sizeof(MQTT_username)-1);
  Serial.print("count - ");
  Serial.println(count);
  count++;

  for(i=count; i<(sizeof(MQTT_username)-1+count); i++){        // username
    temp_packet[i] = MQTT_username[i-count];
  }
  count = i;

  temp_packet[count] = 0x00;                                  //password length
  count++;
  temp_packet[count] = (sizeof(MQTT_password)-1);
  count++;
 

  for(i=count; i<(sizeof(MQTT_password)-1+count); i++){        // password
    temp_packet[i] = MQTT_password[i-count];
  }
  count = i;

  
  temp_packet[1] = count-2;

//  for(j=0;j<count;j++){
//  Serial.print(temp_packet[j]);
//  }
//  Serial.println();
//  Serial.print("count - ");
//  Serial.println(count);
  
  conn_packet_size = count;
  return temp_packet;
}

byte* make_sub_packet(){
  static byte temp_packet[100];
  int count=0;
  int i =0;
  int j =0;
  temp_packet[0] = 0x82;                                      //Packet identifier 82 (SUB)
  temp_packet[1] = 0x0D;                                      //RL
  temp_packet[2] = 0x00;                                      //PktID
  temp_packet[3] = 0x01;

  temp_packet[4] = 0x00;                                      //TPLEN
  temp_packet[5] = 0x08;                                      //
  
  temp_packet[6] = 0x44;                                      //D
  temp_packet[7] = 0x65;                                      //e
  temp_packet[8] = 0x76;                                      //v
  temp_packet[9] = 0x2F;                                      ///
  temp_packet[10] = 0x54;                                     //T
  temp_packet[11] = 0x65;                                     //e
  temp_packet[12] = 0x73;                                     //s
  temp_packet[13] = 0x74;                                     //t

  temp_packet[14] = 0x00 ;                                         //Qos

  
  
  sub_packet_size = 15;
  return temp_packet;
}

byte* make_publish_packet(){
  
  static byte pub_packet[2000];
  int count=0;
  int i =0;
  int j =0;
  
  if(QoS ==1){
    pub_packet[0] = 0x32;                                     //Packet identifier 32 (Publish)
  }else{
    pub_packet[0] = 0x30;
  }
  
  pub_packet[3] = 0x00;                                       // Topic length
  pub_packet[4] = (sizeof(topic)-1);
  
  for(count=5; count<(sizeof(topic)-1+5); count++){            // Client ID
    pub_packet[count] = topic[count-5];
  }
  Serial.print("count after topic- ");
  Serial.println(count);

  if(QoS ==1){
    pub_packet[count] = 0x00;                                 // Topic length
    count++;
    pub_packet[count] = 0x04;
    count++;
  }

  for(i=count; i<(sizeof(message)-1+count); i++){              // payload
    pub_packet[i] = message[i-count];
  }
  count = i;

  pub_packet[1] = (count-3) | 0b10000000;                                    //Remaining length
  pub_packet[2] = (count-3) >>7;

//  for(j=0;j<count;j++){
//  Serial.print(temp_packet[j]);
//  }
//  Serial.println();
//  Serial.print("count - ");
//  Serial.println(count);
  
  pub_packet_size = count;                                      
  return pub_packet;
  
}

void write_pub_packet(){
  char checkB[20];
  uint8_t serC = 0;
  int dys = 0;

  paraValues[55]= random(0,5000);                     //variable for supply voltage
  paraValues[1]= random(0,5000);  
  paraValues[2]= random(0,5000);  
  paraValues[3]= random(0,5000); 
  
//  if(serC) { Serial.println("Debug ON"); }
//  
//  Serial.print("\t GPS: "); Serial.println(gpsRead);
//
//  if(!gpsRead) 
//  {
//    getGPS();
//  }
//  else
//  {
//    paraValues[57] = ((atof(latit))*1000000);
//    paraValues[58] = ((atof(longit))*1000000);
////  }
//  
//  serPrint = 0;
//  
//  if(triggered != 3)
//  {
//    Serial.println(" ");Serial.println("*state,-> Server Update");
//          
//    if(modCan == 1)
//    {
//      modbusRead(0); modEnbit = 0; 
//    }
//    
    for (int op=0; op<49; op++)
    {
      dys += sprintf(checkB, "%ld", paraValues[op]); if(serC) { Serial.print(op); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }
      
      if(!modCan && (op > 17) )
      {
        break;
      }
    }
    
    for(uint8_t tr = 49; tr < 54 ; tr++ )
    {
      dys += sprintf(checkB, "%ld", paraValues[tr]); if(serC) { Serial.print(tr); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }
    }
    
    dys += sprintf(checkB, "%u", (((uint32_t)paraValues[62] >> 0 ) & 0xFFFF));      if(serC) { Serial.print("1"); Serial.print(", "); Serial.print("1"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); } // equip condition 
    dys += sprintf(checkB, "%u", (((uint32_t)paraValues[62] >> 16 ) & 0xFFFF));     if(serC) { Serial.print("2"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// device stat
    dtostrf(((float)paraValues[56]/60),0, 2, checkB); dys += strlen(checkB);        if(serC) { Serial.print("3"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// hour meter
    dtostrf(((float)paraValues[57]/1000000),0,6,checkB); dys += strlen(checkB);     if(serC) { Serial.print("4"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// gps location
    dtostrf(((float)paraValues[58]/1000000),0,6,checkB); dys += strlen(checkB);     if(serC) { Serial.print("5"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// gps location
    dtostrf(((float)((paraValues[59] >> 16) & 0xFFFF)*0.01),0,2,checkB); dys += strlen(checkB); if(serC) { Serial.print("6"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// gps speed
    dtostrf(((((float)((paraValues[55] >> 0) & 0xFFFF))*0.0294)+2.2941),0,2,checkB); dys += strlen(checkB); if(serC) { Serial.print("7"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// sup vol
    dys += sprintf(checkB, "%u", (((uint32_t)paraValues[59] >> 0) & 0xFFFF));       if(serC) { Serial.print("8"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// signal strength
    
    dtostrf(((((float)((paraValues[55] >> 0) & 0xFFFF))*0.0294)+2.2941),0,2,checkB); dys += strlen(checkB);     if(serC) { Serial.print("9"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// sup vol
    dtostrf(((((float)((paraValues[55] >> 16) & 0xFFFF))*0.0036)+1.1728),0,2,checkB); dys += strlen(checkB);    if(serC) { Serial.print("10"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// bat vol
    dys += sprintf(checkB, "%lu", (((uint32_t)paraValues[60])));                if(serC) { Serial.print("11"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// unix time
    dys += sprintf(checkB, "%u", (((uint32_t)paraValues[61] >> 0 ) & 0xFFFF));  if(serC) { Serial.print("12"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// offline status
    dys += sprintf(checkB, "%u", (((uint32_t)paraValues[62] >> 0 ) & 0xFFFF));  if(serC) { Serial.print("13"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// condition
    dys += sprintf(checkB, "%u", (((uint32_t)paraValues[62] >> 16 ) & 0xFFFF)); if(serC) { Serial.print("14"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// device status
    dys += sprintf(checkB, "%u", (((uint32_t)paraValues[63] >> 0 ) & 0xFFFF));  if(serC) { Serial.print("15"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// analog pins
    dys += sprintf(checkB, "%u", (((uint32_t)paraValues[63] >> 16 ) & 0xFFFF)); if(serC) { Serial.print("16"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// digital pins
    dys += sprintf(checkB, "%lu",(((uint32_t)paraValues[54])));                 if(serC) { Serial.print("17"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// modbus active
    dys += sprintf(checkB, "%u", (((uint32_t)paraValues[59] >> 0 ) & 0xFFFF));  if(serC) { Serial.print("18"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// signal strength
    dys += sprintf(checkB, "%d", (mcc));                                        if(serC) { Serial.print("19"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// MCC
    dys += sprintf(checkB, "%d", (mnc));                                        if(serC) { Serial.print("20"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// mnc
    dys += sprintf(checkB, "%u", (lac));                                        if(serC) { Serial.print("21"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.print(lac); Serial.print(", "); Serial.println(checkB); }// lac
    dys += sprintf(checkB, "%lu", (cellid));                                    if(serC) { Serial.print("22"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// cellid
    dys += sprintf(checkB, "%u", (((uint32_t)paraValues[61] >> 16 ) & 0xFFFF)); if(serC) { Serial.print("23"); Serial.print(", "); Serial.print(dys); Serial.print(", "); Serial.println(checkB); }// OTA bit
    
    if(modCan)
    {
      dys += 893;
    }
    else
    {
      dys += 533; // 501
    }


    ///////////////////////////////////////////////

    if(QoS ==1){
    Serial2.write(0x32);                                     //Packet identifier 32 (Publish)
  }else{
    Serial2.write(0x30); ;
  }

  byte lsb = (dys-3) | 0b10000000;
  byte msb = (dys-3) >>7;
  Serial2.write(lsb);                                         //Remaining length
  Serial2.write(msb);
  
  Serial2.write(0x00);                                        // Topic length
  Serial2.write(sizeof(topic)-1);
  
  Serial2.print(topic);            // Client ID
  

  if(QoS ==1){
    Serial2.write(0x00);Serial2.write(0x04);
  }

  



  /////////////////////////////////////////


  

    Serial2.print("{\"api_token\":\"");Serial2.print(apiToken);Serial2.print("\",\"data_set\":{");  
      for(uint8_t p = 0; p < 13; p++)
      {
        if(p == 0)    { Serial2.print("\"AN01\":");Serial2.print(paraValues[0]); }
        else if(p < 9){ Serial2.print(",\"AN0");Serial2.print(p+1);Serial2.print("\":");Serial2.print(paraValues[p]); }  
        else          { Serial2.print(",\"AN");Serial2.print(p+1);Serial2.print("\":");Serial2.print(paraValues[p]); }       
      }
    
      for(uint8_t q = 13; q < 19; q++)
      {
        Serial2.print(",\"DI0");Serial2.print(q-12);Serial2.print("\":");Serial2.print(paraValues[q]);
      }
    
      if(modCan)  // modbus and canbus paramters paraValue(19 - 48)
      {
        for(uint8_t mb = 19; mb < 49 ; mb++ )
        {
          if(modCan == 1)
          {
            if((mb-18) < 10)
            {
             Serial2.print(",\"MODBUS0");Serial2.print(mb-18);Serial2.print("\":");Serial2.print(paraValues[mb]);
            }
            else
            {
             Serial2.print(",\"MODBUS");Serial2.print(mb-18);Serial2.print("\":");Serial2.print(paraValues[mb]);   
            }
          }
          if(modCan == 2)
          {
            if((mb-18) < 10)
            {
             Serial2.print(",\"CANBUS0");Serial2.print(mb-18);Serial2.print("\":");Serial2.print(paraValues[mb]);
            }
            else
            {
             Serial2.print(",\"CANBUS");Serial2.print(mb-18);Serial2.print("\":");Serial2.print(paraValues[mb]);   
            }
          }
        }
      }
      
      for(uint8_t tr = 49; tr < 54 ; tr++ )
      {
        Serial2.print(",\"LOGIC0");Serial2.print(tr-48);Serial2.print("\":");Serial2.print(paraValues[tr]);
      }
      
      Serial2.print(",\"EQUIP_CONDITION\":"); Serial2.print(((uint32_t)paraValues[62] >> 0 ) & 0xFFFF);
      Serial2.print(",\"DEVICE_STATUS\":");   Serial2.print(((uint32_t)paraValues[62] >> 16) & 0xFFFF);       
      Serial2.print(",\"HOUR_METER\":");      Serial2.print(((float)paraValues[56]/60),2);  
      Serial2.print(",\"GPS_LOCATION\":\"");  Serial2.print(((float)paraValues[57]/1000000),6);Serial2.print(",");Serial2.print(((float)paraValues[58]/1000000),6);Serial2.print("\""); //GPS coordinates
      Serial2.print(",\"GPS_SPEED\":");       Serial2.print(((float)((paraValues[59] >> 16) & 0xFFFF)*0.01),2); 
      Serial2.print(",\"SUPPLY_VOLTAGE\":");  Serial2.print((((float)((paraValues[55] >> 0) & 0xFFFF)*0.0294)+2.2941),2);      
      Serial2.print(",\"SIGNAL_STRENGTH\":"); Serial2.print(((uint32_t)paraValues[59] >> 0) & 0xFFFF);
      Serial2.print("},\"const\":{");     
      Serial2.print("\"SUPV\":");  Serial2.print((((float)((paraValues[55] >> 0) & 0xFFFF)*0.0294)+2.2941),2);    //supply voltage
      Serial2.print(",\"BATV\":"); Serial2.print((((float)((paraValues[55] >> 16) & 0xFFFF)*0.0036)+1.1728),2);   //battery voltage          
      Serial2.print(",\"UTIM\":"); Serial2.print(paraValues[60]);                                                 //unix time
      Serial2.print(",\"DSTA\":"); Serial2.print(((uint32_t)paraValues[61] >> 0) & 0xFFFF);                       //offlinestatus
      Serial2.print(",\"CFLG\":"); Serial2.print(((uint32_t)paraValues[62] >> 0) & 0xFFFF);                       //condition
      Serial2.print(",\"SFLG\":"); Serial2.print(((uint32_t)paraValues[62] >> 16) & 0xFFFF);                      //device status
      Serial2.print(",\"ANLP\":"); Serial2.print(((uint32_t)paraValues[63] >> 0) & 0xFFFF);                       //analog pins  
      Serial2.print(",\"DGIP\":"); Serial2.print(((uint32_t)paraValues[63] >> 16) & 0xFFFF);                      //digital pins    
      Serial2.print(",\"MBAC\":"); Serial2.print(paraValues[54]);                                                 //modbus active      
      Serial2.print(",\"RSSI\":"); Serial2.print(((uint32_t)paraValues[59] >> 0) & 0xFFFF);                       //signal strength
      Serial2.print(",\"NMCC\":"); Serial2.print(mcc);                                                            //mobile country code
      Serial2.print(",\"NMNC\":"); Serial2.print(mnc);                                                            //mobile network code
      Serial2.print(",\"NLAC\":"); Serial2.print(lac);                                                            //location area code
      Serial2.print(",\"NCID\":"); Serial2.print(cellid);                                                         //cell id
      Serial2.print(",\"OTAS\":"); Serial2.print(((uint32_t)paraValues[61] >> 16) & 0xFFFF);                      //OTA bit
      Serial2.print("}}");
}

void sim800startup(){
  pinMode (6, OUTPUT);
  digitalWrite(6, HIGH);
  delay (3000);
  digitalWrite(6, LOW);
  delay(2000);
  
  Serial2.begin(9600);
  Serial.println("AT+IPR=9600");
  Serial2.println("AT+IPR=9600");
  delay(20000);
  Serial.println("AT+SAPBR=3,1,\"Contype\", \"GPRS\"");
  Serial2.println("AT+SAPBR=3,1,\"Contype\", \"GPRS\"");
  delay(1000);
  Serial.println("AT+SAPBR=3,1,\"APN\",\"mobitel\"");
  Serial2.println("AT+SAPBR=3,1,\"APN\",\"mobitel\"");
  delay(1000);
  Serial.println("AT+SAPBR=1,1");
  Serial2.println("AT+SAPBR=1,1");
  delay(1000);
//  Serial.println("AT+CIPMODE=1");
//  Serial2.println("AT+CIPMODE=1");
//  delay(1000);
  Serial.println("AT+CSTT=\"mobitel\"");
  Serial2.println("AT+CSTT=\"mobitel\"");
  delay(1000);
  Serial.println("AT+CIICR");
  Serial2.println("AT+CIICR");
  delay(1000);
  Serial.println("AT+CIPSSL=1");
  Serial2.println("AT+CIPSSL=1");
  delay(1000);
  Serial.println("AT+CIFSR");
  Serial2.println("AT+CIFSR");
  delay(1000);
}

void setup() {
  // initialize both serial ports:
  Serial.begin(9600);
  Serial.println("Start");
  sim800startup();

 
  
//  write_pub_packet();

  //byte* pub_packet;
  //pub_packet = make_publish_packet();
  //Serial.println();
  //Serial.print("pub_packet_size - ");
  //Serial.println(pub_packet_size);
  //for(int i=0; i<pub_packet_size;i++){
  //  Serial.write(*(pub_packet+i));
  //}
  //Serial.println();
  //Serial.println();
  //Serial2.write(publish_qos1, sizeof(publish_qos1));
  //
}

void loop() {
    // read from port 0, send to port 1:
  handleSerial();
  
  // read from port 1, send to port 0:
  if (Serial2.available()) {
    int inByte = Serial2.read();
    Serial.write(inByte);
  }


}

void handleSerial() {
 while (Serial.available() > 0) {
   char inByte;
   int lfcount=0; 
   int count =0;
   char sendresponse[16];
   char conack[4];
   int sendresponse_count= 0;
   char incomingCharacter = Serial.read();
   switch (incomingCharacter) {
     case 't':                                                              // Start TCP
      Serial2.print("AT+CIPSTART=\"TCP\",\"");
      Serial2.print(server);
      Serial2.print("\",\"");
      Serial2.print(port);
      Serial2.println("\"");
//      delay(1000);
      break;
    
     case 'c':                                          ///////////////////// CONNECT packet
      Serial2.println("AT+CIPSEND");
      while (Serial2.available() == 0);
      
      byte* conn_packet;          
      conn_packet = make_conn_packet();                                     // Generating CONN packet
      for(int i=0; i<conn_packet_size;i++){
      Serial2.write(*(conn_packet+i));
      }
//      Serial2.write(mqttconnect, sizeof(mqttconnect));

      Serial2.write(0x1A);
      Serial.print("Connecting to MQTT Broker........");
      while(Serial2.available() || count<500){
        count++;
        inByte = Serial2.read();
        Serial.write(inByte);
        if (lfcount == 0){
          if (inByte == 10){
            Serial.print("Sending connect packet ....   ");
            lfcount =1;
          }
        }else if (lfcount == 1){
          if (inByte == 26){
            Serial.println();
            Serial.print("Sent ....   ");
            delay(1000);
            lfcount =2;
          }
                      
        }else if (lfcount == 2){
          if (inByte == 10){
            lfcount =3;
          }          
        }else if (lfcount == 3){
          if (inByte == 10){
            sendresponse[sendresponse_count-1]=0;
            lfcount =4;
            if(!strcmp(sendresponse, "SEND OK")){
              Serial.println("-SEND OK");
            }else{
              Serial.println("-SEND FAILED, Please Resend");
            }
          }
          sendresponse[sendresponse_count]=inByte;
          sendresponse_count++;          
        }else if (lfcount == 4){
          if(inByte == 32){
            inByte = Serial2.read();
            if(inByte == 2){
              inByte = Serial2.read();
              if(inByte == 0){
                inByte = Serial2.read();
                if(inByte == 0){
                  Serial.println("Connection accepted");                  
                }else if(inByte == 1){
                  Serial.println("Connection Refused, unacceptable protocol version");
                }else if(inByte == 2){
                  Serial.println("Connection Refused, identifier rejected");
                }else if(inByte == 3){
                  Serial.println("Connection Refused, Server unavailable");
                }else if(inByte == 4){
                  Serial.println("Connection Refused, bad user name or password");
                }else if(inByte == 5){
                  Serial.println("Connection Refused, not authorized");
                }
                
              }
            }
            
          }else{
            Serial.println("Connection not acknowledged");
          }
            break;
          }
      }
      Serial.println("#");
      break;

      case 'p':                    //////////////////////////////////////////// Publish packet
      Serial2.println("AT+CIPSEND");
      while (Serial2.available() == 0);

//      byte* pub_packet;
//      pub_packet = make_publish_packet();                                   //Generating Publish packet
//      for(int i=0; i<pub_packet_size;i++){
//        Serial2.write(*(pub_packet+i));
//      }
       write_pub_packet();

//      Serial.print ("size of pub - ");
//      Serial.println(pub_packet_size);
//      Serial2.write(publish_qos1, sizeof(publish_qos1));

      
      Serial2.write(0x1A);
      Serial.print("Publishing to server........");
      while(Serial2.available() || count<1200){
        count++;
        inByte = Serial2.read();
        Serial.write(inByte);
        if (lfcount == 0){
          if (inByte == 10){
            Serial.println("Sending connect packet ....   ");
            lfcount =1;
          }
        }else if (lfcount == 1){
          if (inByte == 26){
            Serial.println();
            Serial.print("Sent ....   ");
            delay(1000);
            lfcount =2;
          }
                      
        }else if (lfcount == 2){
          if (inByte == 10){
            lfcount =3;
          }          
        }else if (lfcount == 3){
          if (inByte == 10){
            sendresponse[sendresponse_count-1]=0;
            lfcount =4;
            if(!strcmp(sendresponse, "SEND OK")){
              Serial.println("-SEND OK");
            }else{
              Serial.println("-SEND FAILED, Please Resend");
            }
          }
          sendresponse[sendresponse_count]=inByte;
          sendresponse_count++;  
                  
        }else if (lfcount == 4){
          if(inByte == 64){
            inByte = Serial2.read();
            if(inByte == 2){
              inByte = Serial2.read();
              if(inByte == 0){
                inByte = Serial2.read();
                if(inByte == 4){
                  Serial.println("Publish acknowledged");                  
                }
                
              }
            }
          }
            break;
          }
      }
      Serial.println("#");
      break;

      case 's':
      Serial2.println("AT+CIPSEND");
      while (Serial2.available() == 0);

      byte* sub_packet;
      sub_packet = make_sub_packet();                                   //Generating Publish packet
      for(int i=0; i<sub_packet_size;i++){
        Serial2.write(*(sub_packet+i));
      }
//       write_pub_packet();

//      Serial.print ("size of pub - ");
//      Serial.println(pub_packet_size);
//      Serial2.write(publish_qos1, sizeof(publish_qos1));

      
      Serial2.write(0x1A);
      Serial.print("Publishing to server........");
    }
 }
}




