/*
  BlackBox MQTT testing firmware

  Creates connection and publish packets and publishes to the server through MQTT

  Amjed Nizam
  Utech
*/

//Server details
char server[] = "utechconnect.tk";
char port[] = "8883";

/////// CONN packet variables
char client_ID[] = "ABCDEF";          //!!cannot be 26 letters!!
char MQTT_username[] = "utechiot";
char MQTT_password[] = "Utech@1";
int keep_alive_time = 120;            //session keep alive time in seconds
int conn_packet_size = 0;             //global variable. updated in make_conn_packet()

/////// PUBLISH packet variables
byte QoS = 1;
char topic[]= "messages/4ngs2FL7iCHIdtyUdAXi";
char message[] = "{\"data_set\":{\"temperature\":31,\"humidity\":50}}";
int pub_packet_size;


//byte mqttconnect[] = {0x10,0x25,0x00,0x04,0x4D,0x51,0x54,0x54,0x04,0xC2,0x00,0x78,0x00,0x06,0x41,0x42,0x43,0x44,0x45,0x46,0x00,0x08,0x75,0x74,0x65,0x63,0x68,0x69,0x6F,0x74,0x00,0x07,0x55,0x74,0x65,0x63,0x68,0x40,0x31};
//byte publish_qos1[] = {0x32,0x47,0x00,0x16,0x6D,0x65,0x73,0x73,0x61,0x67,0x65,0x73,0x2F,0x54,0x31,0x5F,0x54,0x45,0x53,0x54,0x5F,0x54,0x4F,0x4B,0x45,0x4E,0x00,0x04,0x7B,0x22,0x64,0x61,0x74,0x61,0x5F,0x73,0x65,0x74,0x22,0x3A,0x7B,0x22,0x74,0x65,0x6D,0x70,0x65,0x72,0x61,0x74,0x75,0x72,0x65,0x22,0x3A,0x31,0x36,0x2C,0x22,0x68,0x75,0x6D,0x69,0x64,0x69,0x74,0x79,0x22,0x3A,0x32,0x37,0x7D,0x7D};

byte* make_conn_packet(){
  static char temp_packet[100];
  int count=0;
  int i =0;
  int j =0;
  temp_packet[0] = 0x10;                                      //Packet identifier 10 (CONN)

  temp_packet[2] = 0x00;                                      //Protocol length
  temp_packet[3] = 0x04;

  temp_packet[4] = 0x4D;                                      //M
  temp_packet[5] = 0x51;                                      //Q
  temp_packet[6] = 0x54;                                      //T
  temp_packet[7] = 0x54;                                      //T

  temp_packet[8] = 0x04;                                      //Protocol level

  temp_packet[9] = 0xC2;                                      //Flag bit

  temp_packet[10] = 0x00;                                     //Keep alive
  temp_packet[11] = keep_alive_time;

  temp_packet[12] = 0x00;                                     // Client ID length
  temp_packet[13] = (sizeof(client_ID)-1);
  
  for(count=14; count<(sizeof(client_ID)-1+14); count++){        // Client ID
    temp_packet[count] = client_ID[count-14];
  }
  Serial.print("count - ");
  Serial.println(count);
  
  temp_packet[count] = 0x00;                                  //username length
  count++;
  temp_packet[count] = (sizeof(MQTT_username)-1);
  Serial.print("count - ");
  Serial.println(count);
  count++;

  for(i=count; i<(sizeof(MQTT_username)-1+count); i++){        // username
    temp_packet[i] = MQTT_username[i-count];
  }
  count = i;

  temp_packet[count] = 0x00;                                  //password length
  count++;
  temp_packet[count] = (sizeof(MQTT_password)-1);
  count++;
 

  for(i=count; i<(sizeof(MQTT_password)-1+count); i++){        // password
    temp_packet[i] = MQTT_password[i-count];
  }
  count = i;


  
  temp_packet[1] = count-2;

//  for(j=0;j<count;j++){
//  Serial.print(temp_packet[j]);
//  }
//  Serial.println();
//  Serial.print("count - ");
//  Serial.println(count);
  
  conn_packet_size = count;
  return temp_packet;
}


byte* make_publish_packet(){
  
  static char temp_packet[100];
  int count=0;
  int i =0;
  int j =0;
  
  if(QoS ==1){
    temp_packet[0] = 0x32;                                     //Packet identifier 32 (Publish)
  }else{
    temp_packet[0] = 0x30;
  }
  
  temp_packet[2] = 0x00;                                       // Topic length
  temp_packet[3] = (sizeof(topic)-1);
  
  for(count=4; count<(sizeof(topic)-1+4); count++){            // Client ID
    temp_packet[count] = topic[count-4];
  }
  Serial.print("count after topic- ");
  Serial.println(count);

  if(QoS ==1){
    temp_packet[count] = 0x00;                                 // Topic length
    count++;
    temp_packet[count] = 0x04;
    count++;
  }

  for(i=count; i<(sizeof(message)-1+count); i++){              // payload
    temp_packet[i] = message[i-count];
  }
  count = i;

  temp_packet[1] = count-2;

//  for(j=0;j<count;j++){
//  Serial.print(temp_packet[j]);
//  }
//  Serial.println();
//  Serial.print("count - ");
//  Serial.println(count);
  
  pub_packet_size = count;
  return temp_packet;
  
}
void setup() {
  // initialize both serial ports:
  pinMode (6, OUTPUT);
  digitalWrite(6, HIGH);
  delay (3000);
  digitalWrite(6, LOW);
  delay(2000);
  Serial.begin(9600);
  Serial.println("Start");
  Serial2.begin(9600);
  Serial.println("AT+IPR=9600");
  Serial2.println("AT+IPR=9600");
  delay(20000);
  Serial.println("AT+SAPBR=3,1,\"Contype\", \"GPRS\"");
  Serial2.println("AT+SAPBR=3,1,\"Contype\", \"GPRS\"");
  delay(1000);
  Serial.println("AT+SAPBR=3,1,\"APN\",\"mobitel\"");
  Serial2.println("AT+SAPBR=3,1,\"APN\",\"mobitel\"");
  delay(1000);
  Serial.println("AT+SAPBR=1,1");
  Serial2.println("AT+SAPBR=1,1");
  delay(1000);
//  Serial.println("AT+CIPMODE=1");
//  Serial2.println("AT+CIPMODE=1");
//  delay(1000);
  Serial.println("AT+CSTT=\"mobitel\"");
  Serial2.println("AT+CSTT=\"mobitel\"");
  delay(1000);
  Serial.println("AT+CIICR");
  Serial2.println("AT+CIICR");
  delay(1000);
  Serial.println("AT+CIPSSL=1");
  Serial2.println("AT+CIPSSL=1");
  delay(1000);
  Serial.println("AT+CIFSR");
  Serial2.println("AT+CIFSR");
  delay(1000);
//  Serial.println("AT+CIPSTART=\"TCP\",\"utechconnect.tk\",\"8883\"");
//  Serial2.println("AT+CIPSTART=\"TCP\",\"utechconnect.tk\",\"8883\"");
//  delay(1000);
//byte* pub_packet;
//pub_packet = make_publish_packet();
//Serial.println();
//Serial.print("pub_packet_size - ");
//Serial.println(pub_packet_size);
//for(int i=0; i<pub_packet_size;i++){
//  Serial.write(*(pub_packet+i));
//}
//Serial.println();
//Serial.println();
//Serial2.write(publish_qos1, sizeof(publish_qos1));



}

void loop() {
    // read from port 0, send to port 1:
  handleSerial();
  
  // read from port 1, send to port 0:
  if (Serial2.available()) {
    int inByte = Serial2.read();
    Serial.write(inByte);
  }


}

void handleSerial() {
 while (Serial.available() > 0) {
   char inByte;
   int lfcount=0; 
   int count =0;
   char sendresponse[16];
   char conack[4];
   int sendresponse_count= 0;
   char incomingCharacter = Serial.read();
   switch (incomingCharacter) {
     case 't':                                                              // Start TCP
      Serial2.print("AT+CIPSTART=\"TCP\",\"");
      Serial2.print(server);
      Serial2.print("\",\"");
      Serial2.print(port);
      Serial2.println("\"");
//      delay(1000);
      break;
 
     case 'c':                                                              // connect packet
      Serial2.println("AT+CIPSEND");
      while (Serial2.available() == 0);
      
      byte* conn_packet;
      conn_packet = make_conn_packet();
      for(int i=0; i<conn_packet_size;i++){
      Serial2.write(*(conn_packet+i));
      }
//      Serial2.write(mqttconnect, sizeof(mqttconnect));
      Serial2.write(0x1A);
      Serial.print("Connecting to MQTT Broker........");
      while(Serial2.available() || count<500){
        count++;
        inByte = Serial2.read();
        Serial.write(inByte);
        if (lfcount == 0){
          if (inByte == 10){
            Serial.print("Sending connect packet ....   ");
            lfcount =1;
          }
        }else if (lfcount == 1){
          if (inByte == 26){
            Serial.println();
            Serial.print("Sent ....   ");
            delay(1000);
            lfcount =2;
          }
                      
        }else if (lfcount == 2){
          if (inByte == 10){
            lfcount =3;
          }          
        }else if (lfcount == 3){
          if (inByte == 10){
            sendresponse[sendresponse_count-1]=0;
            lfcount =4;
            if(!strcmp(sendresponse, "SEND OK")){
              Serial.println("-SEND OK");
            }else{
              Serial.println("-SEND FAILED, Please Resend");
            }
          }
          sendresponse[sendresponse_count]=inByte;
          sendresponse_count++;          
        }else if (lfcount == 4){
          if(inByte == 32){
            inByte = Serial2.read();
            if(inByte == 2){
              inByte = Serial2.read();
              if(inByte == 0){
                inByte = Serial2.read();
                if(inByte == 0){
                  Serial.println("Connection accepted");                  
                }else if(inByte == 1){
                  Serial.println("Connection Refused, unacceptable protocol version");
                }else if(inByte == 2){
                  Serial.println("Connection Refused, identifier rejected");
                }else if(inByte == 3){
                  Serial.println("Connection Refused, Server unavailable");
                }else if(inByte == 4){
                  Serial.println("Connection Refused, bad user name or password");
                }else if(inByte == 5){
                  Serial.println("Connection Refused, not authorized");
                }
                
              }
            }
            
          }else{
            Serial.println("Connection not acknowledged");
          }
            break;
          }
      }
      Serial.println("#");
      break;

      case 'p':                                                              // connect packet
      Serial2.println("AT+CIPSEND");
      while (Serial2.available() == 0);

      byte* pub_packet;
      pub_packet = make_publish_packet();
      for(int i=0; i<pub_packet_size;i++){
        Serial2.write(*(pub_packet+i));
      }
//      Serial2.write(publish_qos1, sizeof(publish_qos1));

      
      Serial2.write(0x1A);
      Serial.print("Publishing to server........");
      while(Serial2.available() || count<500){
        count++;
        inByte = Serial2.read();
        Serial.write(inByte);
        if (lfcount == 0){
          if (inByte == 10){
            Serial.print("Sending connect packet ....   ");
            lfcount =1;
          }
        }else if (lfcount == 1){
          if (inByte == 26){
            Serial.println();
            Serial.print("Sent ....   ");
            delay(1000);
            lfcount =2;
          }
                      
        }else if (lfcount == 2){
          if (inByte == 10){
            lfcount =3;
          }          
        }else if (lfcount == 3){
          if (inByte == 10){
            sendresponse[sendresponse_count-1]=0;
            lfcount =4;
            if(!strcmp(sendresponse, "SEND OK")){
              Serial.println("-SEND OK");
            }else{
              Serial.println("-SEND FAILED, Please Resend");
            }
          }
          sendresponse[sendresponse_count]=inByte;
          sendresponse_count++;  
                  
        }else if (lfcount == 4){
          if(inByte == 64){
            inByte = Serial2.read();
            if(inByte == 2){
              inByte = Serial2.read();
              if(inByte == 0){
                inByte = Serial2.read();
                if(inByte == 4){
                  Serial.println("Publish acknowledged");                  
                }
                
              }
            }
          }
            break;
          }
      }
      Serial.println("#");
      break;
    }
 }
}




